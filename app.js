const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');
var session = require('express-session');
// Routes Import
const home = require('./routes/home');
// const contact = require('./routes/contact');
// const service = require('./routes/service');
// const about = require('./routes/about');

// Database
const db = require('./config/database');
//const auth = require('./middleware/auth');
//const { allowedNodeEnvironmentFlags } = require('process');

// Test DB
db.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(err => console.log('Error: ' + err))

const app = express();
// Handlebars
app.set('view engine', 'hbs');

// Body Parser
app.use(express.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));
// IITech  route setup

// Routes
app.use('/', home);
// app.use('/contact', contact);
// app.use('/service', service);
// app.use('/about', about);
// Index route
const PORT = process.env.PORT || 4000;

app.listen(PORT, console.log(`Server started on port ${PORT}`));
