const express = require('express'); 
const router = express.Router();


router.get('/', async(req, res) => {
  res.render('home', {layout:'partials/home_layout.hbs'});
})
router.get('/blog', async(req, res) => {
  res.render('blog', {layout:'partials/home_layout.hbs'});
})
router.get('/feature', async(req, res) => {
  res.render('feature', {layout:'partials/home_layout.hbs'});
})

router.get('/team', async(req, res) => {
  res.render('team', {layout:'partials/home_layout.hbs'});
})

router.get('/testimonial', async(req, res) => {
  res.render('testimonial', {layout:'partials/home_layout.hbs'});
})
router.get('/quote', async(req, res) => {
  res.render('quote', {layout:'partials/home_layout.hbs'});
})

router.get('/detail', async(req, res) => {
  res.render('detail', {layout:'partials/home_layout.hbs'});
})

router.get('/service', async(req, res) => {
  res.render('service', {layout:'partials/home_layout.hbs'});
})
router.get('/contact', async(req, res) => {
  res.render('contact.hbs', {layout:'partials/home_layout.hbs'});
})

router.get('/portfolio', async(req, res) => {
  res.render('portfolio.hbs', {layout:'partials/home_layout.hbs'});
})

router.get('/about', async(req, res) => {
  res.render('about.hbs', {layout:'partials/home_layout.hbs'});
})

router.get('/Products', async(req, res) => {
  res.render('Products.hbs', {layout:'partials/home_layout.hbs'});
})




module.exports = router;
